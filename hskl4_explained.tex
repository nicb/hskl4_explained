\documentclass{scrartcl}
\usepackage[pdftex]{hyperref}
\usepackage{xspace}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{mathtools}
\usepackage{amssymb}
\usepackage{varioref}
\usepackage{needspace}
\usepackage{bm}
\usepackage[mark]{gitinfo2}
\usepackage{paralist}

\title{A detailed explanation of Hyung-Suk Kim's section 4 maths\\[0.5\baselineskip]\tiny{(v.\rel)}}
\author{Nicola Bernardini}
\date{~}

\newcommand{\imagedir}{./images}
\newcommand{\bmrm}[1]{\bm{\mathrm{#1}}}
\newcommand{\rel}{\gitAbbrevHash\gitDirty\ \gitCommitterIsoDate\xspace}
\newcommand{\mppinv}{Moore--Penrose pseudoinverse\xspace}
\newcommand{\svd}{singular value decomposition\xspace}

\input{values}

\setlength{\parindent}{0pt}
\setlength{\parskip}{0.5\baselineskip}

\begin{document}

\maketitle
\hfill\emph{To Francesco and Babak}

\input{sect1_introduction}

\input{sect2_eq3b}

\input{sect3_s4}

\section{The \mppinv\label{sec:mppinv}}

Unfortunately, Kim explicits the \emph{\mppinv} in a
simplified way and it doesn't work.

This is the way Kim presents this pseudo--inversion:

\begin{equation}
	\bmrm{M}^{\dagger} = (\bmrm{M}^{T} \cdot \bmrm{M})^{-1} \cdot \bmrm{M}^{T}
\end{equation}

This way of doing the inversion does not work.
Checking the way the pseudoinversion is done on
\href{https://en.wikipedia.org/wiki/Moore%E2%80%93Penrose_inverse}{wikipedia}
we understand that the \emph{\mppinv} must be calculated as follows:

\begin{compactenum}

	\item perform a \emph{\svd} of $\bmrm{A}$ (cf.\vref{sec:svd})\label{it:step_one}
	
	\item take the elements of the decomposition and recombine them as
	such: if the \emph{\svd} is $\mathrm{svd}(\bmrm{A}) = \bmrm{U} \cdot \bmrm{D} \cdot \bmrm{V}^{*}$
	(where $\bmrm{U}$, $\bmrm{D}$, and $\bmrm{V}$ are three different matrices (cf.\vref{sec:svd}) and
	$\bmrm{V}^{*}$ is the \emph{conjugate transpose} of matrix $\bmrm{V}$\footnote{
		the conjugate transpose is equal to a simple transpose when
		values are real (as it is in our case)
	}, then

	\begin{equation}
		\bmrm{A}^{\dagger} = \bmrm{V} \cdot \bmrm{D}^{\dagger} \cdot \bmrm{U}^{*}\label{eq:mppinv_with_svd}
	\end{equation}

	where $\bmrm{D}^{\dagger}$ is the pseudoinverse of $\bmrm{D}$ and can be obtained by
	transposing $\bmrm{D}$ and replacing all non--zero values with their
	multiplicative inverses.\label{it:step_two}

\end{compactenum}

If the \emph{\mppinv} works and it is actually correct,
it has to satisfy the following conditions\footnote{
recalling that matrix multiplication is \emph{not} commutative}:

\begin{compactenum}\label{mppinv:conditions}

	\item $\bmrm{A} \cdot \bmrm{A}^{\dagger} \cdot \bmrm{A} = \bmrm{A}$\label{it:cond_one}

	\item $\bmrm{A}^{\dagger} \cdot \bmrm{A} \cdot \bmrm{A}^{\dagger} = \bmrm{A}^{\dagger}$\label{it:cond_two}

	\item $(\bmrm{A} \cdot \bmrm{A}^{\dagger})^{*} = \bmrm{A} \cdot \bmrm{A}^{\dagger}$\label{it:cond_three}

	\item $(\bmrm{A}^{\dagger} \cdot \bmrm{A})^{*} = \bmrm{A}^{\dagger} \cdot \bmrm{A}$\label{it:cond_four}

\end{compactenum}

so it easy to check whether it works or not.

\subsection{The \svd\label{sec:svd}}

We will not get too deep into an explanation of \emph{\svd} for the sake of
preserving our sanity and avoiding to do an entire linear algebra course in
this short paper. We will not unroll it and we will use the
\texttt{scipy.linalg} \texttt{svd} function to perform it. We will just
describe here what the \emph{\svd} is and what it does.

Following again \href{https://en.wikipedia.org/wiki/Singular_value_decomposition}{wikipedia},
in the field of real numbers
\emph{\svd} is the factorization (that is the subdivision) of a matrix $\bmrm{M} \in \mathbb{R}_{N \times M}$ in
the dot product of three different real--valued sub--matrices

\begin{equation}
	\mathrm{svd}(\bmrm{M}) = \bmrm{U} \cdot \bmrm{D} \cdot \bmrm{V}^{*}\label{eq:svd}
\end{equation}

where:

\begin{compactitem}

	\item $\bmrm{U}$ is a \emph{unitary matrix} $\in \mathbb{R}_{M \times M}$\label{it:svd_one}

	\item $\bmrm{D}$ is a \emph{rectangular matrix} $\in \mathbb{R}_{M \times N}$
	      with non--negative real numbers on its diagonal\label{it:svd_two}

	\item $\bmrm{V}$ is another \emph{unitary matrix} $\in \mathbb{R}_{N \times N}$. Since
	      the values are real numbers, its $\bmrm{V}^{*}$ is in fact its
	      transposition $\bmrm{V}^{T}$\label{it:svd_three}

\end{compactitem}

\emph{Unitary matrices} are square matrices whose transposition (in the real numbers domain) is identical
to their inversion, that is

\begin{equation}
	\bmrm{U} \cdot \bmrm{U}^{T} = \bmrm{U}^{T} \cdot \bmrm{U} = \bmrm{I}\label{eq:unitary_matrix}
\end{equation}

where $\bmrm{I}$ is of course the \emph{identity matrix}.

One of the (minor) problems of \emph{\svd}s is that they are not
\emph{unique}: there can be several \emph{\svd}s for the same matrix, and they
all respect all the conditions above.
However, different \emph{\svd}s of the same matrix are equivalent, and
all of them will work for the \emph{\mppinv}.

\needspace{6\baselineskip}
\input{st_svd_ex}

\needspace{20\baselineskip}
\input{sect5_our_case}

\needspace{20\baselineskip}
\input{sect6_reconstructing}

\end{document}
