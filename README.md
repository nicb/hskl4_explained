# *hskl4_explained*: A tutorial on Section 4 of the article by Hyung-Suk Kim on linear predictive coding

![test](https://gitlab.com/nicb/hskl4_explained/badges/master/pipeline.svg)

In the nice article by Hyung-Suk Kim about [*linear predictive coding*](https://ccrma.stanford.edu/~hskim08/lpc/)
there is a section 4 loaded with not-everyday linear algebra math. In order to understand it thus being able
to manipulate it, I wrote this little contrived example basically using the same math.
The example comes with some python code of it which (supposedly) makes it easier to
read and understand.

## License

* text: [![CC BY-SA 4](https://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png){width=8%}](./LICENSE.text)
* code: [![GPL-3](https://www.gnu.org/graphics/gplv3-with-text-136x68.png){width=8%}](./LICENSE.code)
