import os,sys
import matplotlib.pyplot as plt
import hskl4.seq as hseq

seq = hseq.generate()
print(hseq.latex(seq))

imgfile = os.path.splitext(os.path.basename(sys.argv[0]))[0] + ".png"
plt.stem(seq)
plt.savefig("../images/%s" % (imgfile))
