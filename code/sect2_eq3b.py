import os,sys
import pdb
import numpy as np
import scipy as sc
import matplotlib.pyplot as plt

import hskl4           as h
import hskl4.template  as t
import hskl4.seq       as hseq
import hskl4.matricize as m

tmpl = t.Template(__file__)
D = {}

s = hseq.generate()
mtx = m.matricize(s)
D['matricized_x'] = m.latex(s, mtx)

print(tmpl.render(D))
