import hskl4 as h


print("%\n% Global values used in the text\n%")
print("\\newcommand{\\numsamples}{%d\\xspace}" % (h.get_source_sample_no()))
print("\\newcommand{\\npoles}{%d\\xspace}" % (h.get_npoles()))
