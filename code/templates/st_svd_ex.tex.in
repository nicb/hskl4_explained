%
% This is supposed to be inserted as a subsection in the text
%
\subsection{A simple trivial example}

Let's create another simple example with a {{M_dims}} matrix:

{{M}}

\needspace{5\baselineskip}
Its \emph{\svd} will return

{{SVD_results}}

As expected, both $\bmrm{U}$ and $\bmrm{V}$ are \emph{unitary matrices},
because

{{U_dot_UT_results}}

and

{{V_dot_VT_results}}

while the matrix $\bmrm{D}$ is actually a rectangular {{D_dims}} matrix with
only positive values on its diagonal (cf. the third equation in (\vref{eq:ex_svd})).

We can now apply the \emph{\mppinv} on the matrix $\bmrm{M}$,
starting from eq.\vref{eq:mppinv_with_svd}.

{{M_mppinv_with_svd}}

where $\bmrm{D}^{\dagger}$ is

{{D_inv}}

We can now check whether this pseudoinverse satisfies the four conditions
required in \vref{mppinv:conditions}.

\needspace{5\baselineskip}
\begin{compactenum}

	\needspace{5\baselineskip}
	\item $\bmrm{A} \cdot \bmrm{A}^{\dagger} \cdot \bmrm{A} = \bmrm{A}$
	      {{mppinv_cond_one}}

	\needspace{5\baselineskip}
	\item $\bmrm{A}^{\dagger} \cdot \bmrm{A} \cdot \bmrm{A}^{\dagger} = \bmrm{A}^{\dagger}$
	      \begin{small}
	      	{{mppinv_cond_two}}
	      \end{small}

	\needspace{5\baselineskip}
	\item $(\bmrm{A} \cdot \bmrm{A}^{\dagger})^{*} = \bmrm{A} \cdot \bmrm{A}^{\dagger}$
	      \begin{small}
	      	{{mppinv_cond_three}}
	      \end{small}

	\needspace{5\baselineskip}
	\item $(\bmrm{A}^{\dagger} \cdot \bmrm{A})^{*} = \bmrm{A}^{\dagger} \cdot \bmrm{A}$
	      {{mppinv_cond_four}}

\end{compactenum}

We can make a last check with \texttt{scipy.linalg.pinv} to see if we get the
same result:

\begin{small}
{{scipy_linalg_pinv}}
\end{small}

\needspace{18\baselineskip}
We can observe that eq.\vref{eq:scipy_linalg_svd} leads to the same results as our eq.\vref{eq:mppinv_with_svd_ex}.
To better see this identity, we can indeed compare visually the two matrices (Fig.\vref{fig:M_mppinv_check}).

\begin{figure}[h!]
\begin{center}
	\includegraphics[height=0.25\textheight]{\imagedir/st_svd_ex.png}
	\caption{Our simple example MP pseudoinversion vs. \texttt{scipy.linalg.pinv}\label{fig:M_mppinv_check}}
\end{center}
\end{figure}

As we can see from Fig.\vref{fig:M_mppinv_check} the two resulting matrices
are identical -- we actually succeded in calculating the \emph{\mppinv}.
