import os,sys
import pdb
import numpy as np
import scipy as sc
import matplotlib.pyplot as plt

import hskl4.seq       as seq
import hskl4.A_mx      as Amx
import hskl4.latex     as l
import hskl4.mppinv    as mpp
import hskl4.template  as t
import hskl4.pulse     as p
import hskl4.filter    as f

tmpl = t.Template(__file__)
D = {}

#
# calculate the pseudo-inverse of our A matrix
#
A_matrix = Amx.generate()
D['matrix_A'] = Amx.latex(A_matrix)
A_mppinv = mpp.mppinv(A_matrix)
D['A_mppinv'] = l.short_cols(A_mppinv, 0, 4, -4, -1, A_mppinv.shape[0])

#
# get our original sequence, in 1xN (1-rows, N column) order
#
sig = seq.generate()
sigsz = len(sig)
b = sig.T
a = A_mppinv @ b
D['a_factors'] = l.vector(a, lh = '\\bmrm{a} = \\bmrm{A}^{\\dagger} \\cdot \\bmrm{b} = ', label = '\\label{eq:a_factors}', format = '%+.5f')

#
# check with numpy
#
a_sc = np.linalg.lstsq(A_matrix, b, rcond=None)[0]
D['a_factors_with_lstsq'] = l.vector(a_sc, lh = '\\mathrm{numpy.linalg.lstsq(\\bmrm{A}, \\bmrm{b}.T)} = ', format = '%+.5f')

D['sec_len'] = sigsz

print(tmpl.render(D))
