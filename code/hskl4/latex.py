import pdb

def short_rows(m, start, end, ncols):
	result = ""
	for r in range(start, end):
		result += "\t\t"
		for c in range(ncols):
			result += ("%+.3f \\quad " % (m[r][c]))
		result += "\\\\\n"
	return result

def short_cols(m, from0, to0, from1, to1, nrows):
	result = ""
	for r in range(nrows):
		result += "\t\t"
		for c in range(from0, to0):
			result += ("%+.3f \\quad " % (m[r][c]))
		result += "\\ldots "
		for c in range(from1, to1):
			result += ("%+.3f \\quad " % (m[r][c]))
		result += "\\\\\n"
	return result

def vector(v, lh = '', format = '%+.2f', label = '', envs = '\\begin{equation}\n\t', enve = '\n\\end{equation}'):
	result = ('%s%s\\begin{bmatrix}\n\t\t' % (envs, lh))
	for col in v:
		result += (format % (float(col)))
		result += (' \\quad ' if col != v[-1] else '\n')
	result += '\t\\end{bmatrix}%s%s' % (label, enve)
	return result


def matrix(m, lh = '', align = 'r', format = '%+.2f', label = '', envs = '\\begin{equation}\n\t', enve = '\n\\end{equation}'):
	(nr, nc) = m.shape
	result = ("%s%s\\begin{bmatrix}\n\t\t\\begin{array}{" % (envs, lh))
	for col in range(nc):
		result += align
	result += "}\n"
	for row in range(nr):
		result += '\t\t\t'
		for col in range(nc):
			result += (format % m[row][col])
			result += (' & ' if col != nc-1 else '\\\\\n')
	result += ("\t\t\\end{array}%s\n\t\\end{bmatrix}%s" % (label, enve))
	return result

def matrices(m = [], lhs = [], align = 'r', format = '%+.2f', label = ''):
	result = '\\begin{equation}\n'
	result += ('\t\\begin{array}{l c r}\n' if len(lhs) == len(m) else '\t\\begin{array}{l}\n')
	for idx, subm in enumerate(m):
		lh = lhs[idx] if idx < len(lhs) else ''
		result += (matrix(subm, lh = lh, envs = '', enve = ''))
		result += '\\\\\n'
	result += ('\t\\end{array}%s\n\\end{equation}\n' % ( label ))
	return result
		
