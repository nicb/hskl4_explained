import numpy as np
import pdb
import hskl4 as h

def matricize(seq):
	nsamps = h.get_source_sample_no()
	npoles = h.get_npoles()
	output = np.zeros((nsamps, npoles))
	for idx, s in enumerate(seq):
		for p in range(npoles):
			sidx = idx - p - 1
			v = seq[sidx] if sidx >= 0 else 0
			output[idx, p] = v
	return output

def latex(seq, mtx):
	rows = len(seq)
	cols = mtx.shape[1]
	top_rows = 4
	bot_rows = 3
	result = "%\n% generated matrix\n%\n"
	result += "\\begin{equation}\n\\begin{array}{r c r c "
	for c in range(cols):
		result += "l c "
	result += "l }\n"
	for idx, s in enumerate(seq[0:top_rows]):
		result += ("\tx(%d) & = & %+4.2f & = & " % (idx, s))
		for c in range(cols):
			result += ("a_%d \\times %+4.2f & + & " % (c+1, mtx[idx, c]))
		result += (" e(%d) \\\\\n" % (idx))
	result += "\t& & & & etc. \\ldots & & & & & \\\\\n"
	offset = len(seq)-bot_rows
	for idx, s in enumerate(seq[offset:]):
		ridx = idx + offset
		result += ("\tx(%d) & = & %+4.2f & = & " % (ridx, s))
		for c in range(cols):
			result += ("a_%d \\times %+4.2f & + & " % (c+1, mtx[ridx, c]))
		result += ("e(%d) \\\\\n" % (ridx))
	result += "\\end{array}\\label{eq:unrolled}\n\\end{equation}"
	return result

def latex_with_poles(seq, mtx):
	rows = len(seq)
	cols = mtx.shape[1]
	top_rows = 4
	bot_rows = 4
	result = "%\n% generated operational matrix\n%\n"
	result += "\\begin{equation}\n\\begin{array}{r c c"
	for c in range(cols):
		result += "l "
	result += "c }\n"
	for idx, s in enumerate(seq[0:top_rows]):
		result += ("\t\\bmrm{x}_{%2d} & = & [ & " % (idx))
		for c in range(cols):
			result += ("(x(-%d+%d) = %+.2f) & " % (c+1, idx, mtx[idx, c]))
		result += "]\\\\\n"
	result += "\t& & & etc. \\ldots & & & \\\\\n"
	offset = len(seq)-bot_rows
	for idx, s in enumerate(seq[offset:]):
		ridx = idx + offset
		result += ("\t\\bmrm{x}_{%2d} & = & [ & " % (ridx))
		for c in range(cols):
			result += ("(x(-%d+%d) = %+.2f) & " % (c+1, ridx, mtx[ridx, c]))
		result += "]\\\\\n"
	result += "\\end{array}\\label{eq:x_rearranged}\n\\end{equation}"
	return result

def latex_single_case(mtx, idx):
	row = mtx[idx]
	cols = mtx.shape[1]
	result = "\\begin{equation}\n\t[~"
	for c in range(cols):
		result += "%+.2f~" % (row[c])
	result += "] \\cdot\\begin{bmatrix}\n"
	for c in range(cols):
		result += "\t\t\ta_{%d}\\\\\n" % (c+1)
	result += "\t\t\t\\end{bmatrix}\\label{eq:mf_torn_apart}\n\\end{equation}\n"
	return result

def latex_single_case_performed(mtx, idx):
	row = mtx[idx]
	cols = mtx.shape[1]
	result = "\\begin{equation}\n\t[~"
	for c in range(cols-1):
		result += "%+.2f \\times a_{%d}~+~" % (row[c], c+1)
	c = cols-1
	result += "%+.2f \\times a_{%d}~] = \\bmrm{y}\\label{eq:mf_torn_apart_explained}\n\\end{equation}" % (row[c], c+1)
	return result
