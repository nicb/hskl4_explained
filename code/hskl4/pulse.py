import numpy as np
import hskl4 as h

def generate(f0, num_partials = 50):
    """
	generate(f0, block_size, sample_rate, num_partials = 50)
	where:
	* sample_rate: sampling rate of the signal
	* num_partials: band-limiting number of partial for the pulse train
    """
    block_size = h.get_source_sample_no()
    sample_rate = block_size
    sinc     = 1.0/float(sample_rate) # sample increment
    base_rad = (2*np.pi*f0)
    k   = np.linspace(0, (block_size/sample_rate)-sinc, block_size)
    sig = np.zeros((1, block_size))
    for n in range(1, num_partials):
    	sig = (sig + np.cos(n*base_rad*k))
    sig /= num_partials

    sig = np.squeeze(sig)

    return (k, sig)
