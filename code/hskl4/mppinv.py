import numpy as np

def invert_non_zero_values(m):
	(nr, nc) = m.shape
	eps = np.finfo(m.dtype).eps
	for row in range(nr):
		for col in range(nc):
			if m[row][col] > eps:
				m[row][col] = 1/m[row][col]
	return m

def crop_matrix_dimensions(large, small): # two tuples (row, column)
	(lr, lc) = large
	(sr, sc) = small
	res_r = sr - lr
	res_c = sc - lc
	res_r = None if res_r >= 0 else res_r
	res_c = None if res_c >= 0 else res_c
	return res_r, res_c

def mppinv(m):
	U, D, Vh = np.linalg.svd(m)
	Dsq = np.zeros(m.shape)
	D_diag = np.diag(D)
	Dsqr, Dsqc = crop_matrix_dimensions(m.shape, D_diag.shape)
	Dsq[:Dsqr,:Dsqc] = np.diag(D)
	D_inv = invert_non_zero_values(Dsq.T)
	M_mppinv = Vh.T @ D_inv @ U.T
	return M_mppinv
