"""
	system variables with getters and setters.

	Also: random seeding stabilizes random extraction
"""

import numpy as np


"""
	source_sample_no:
	* seq: number of samples
"""

source_sample_no = 64

def get_source_sample_no():
	global source_sample_no
	return source_sample_no

def set_source_sample_no(ssn):
	global source_sample_no
	source_sample_no = ssn
	return source_sample_no

"""
	npoles:
	* npoles: number of poles
"""

npoles = 4

def get_npoles():
	global npoles
	return npoles

def set_npoles(p):
	global npoles
	npoles = p
	return npoles
