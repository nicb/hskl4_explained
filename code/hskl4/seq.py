import numpy as np
import hskl4 as h

def generate():
	"""
		seq.generate:
		generates a sequence of n numbers which is aperiodic but not
		randomic (a sum of non-harmonic frequencies
	"""
	freqs = [ [1.4, 0.55, 0.25*np.pi], [2.7, 0.15, 1.8*np.pi], [3.8, 0.22, 0], [4.23, 0.1, 0.1*np.pi] ]
	nsamps = h.get_source_sample_no()
	output = np.zeros(nsamps)
	t = np.linspace(0, 2*np.pi, nsamps)
	for frq, amp, phi in freqs:
		output += (amp*np.sin(frq*t+phi))
	return output

def latex(seq):
	sz = len(seq)
	hsz = int(sz/8.0)
	result = "%\n% generated sequence\n%\n"
	result += "\\begin{equation}\n\\begin{aligned}\n\tx (n) = &\\{~"
	for idx, s in enumerate(seq):
		result += ("%+4.2f" % (s))
		if s != seq[-1]:
			result += ", "
		if (idx != 0) and ((idx % hsz) == 0):
			result += "\\\\\n\t& "
	result += "~\\}\n\\end{aligned}\\label{eq:sequence}\n\\end{equation}"
	return result
