import numpy as np
import pdb
import numpy as np
import scipy.signal as ss
import hskl4 as h
import hskl4.matricize as m

def filter(input, poles):
	p_array = np.concatenate([np.array([-1]), poles])
	output = ss.lfilter([1], p_array, input)
	return output

def latex_teared_apart(input, poles, orig_seq):
	mtx = m.matricize(input)
	output = filter(input, poles)
	rows, cols = mtx.shape 
	top_rows = 4
	bot_rows = 3
	result = "%\n% filtering teared out matrix\n%\n"
	result += "\\begin{equation}\n\\begin{array}{r c r c l c l c l c l c l c l}\n"
	for idx, i in enumerate(input[0:top_rows]):
		result += ("\tx(%d) & = & %+4.2f & \\approx & " % (idx, orig_seq[idx]))
		for c in range(cols-1):
			result += ("%+4.2f \\times %+4.2f & + & " % (mtx[idx, c], poles[c]))
		c = cols-1
		result += ("%+4.2f \\times %+4.2f " % (mtx[idx, c], poles[c]))
		result += (" = & \\bmrm{y}_{%d} & = & %+4.2f \\\\\n" % (idx, output[idx]))
	result += "\t& & & & etc. \\ldots & & & & & & & & & &\\\\\n"
	offset = len(input)-bot_rows
	for idx, s in enumerate(input[offset:]):
		ridx = idx + offset
		result += ("\tx(%d) & = & %+4.2f & = & " % (ridx, orig_seq[idx]))
		for c in range(cols-1):
			result += ("%+4.2f \\times %+4.2f & + & " % (mtx[ridx, c], poles[c]))
		c = cols-1
		result += ("%+4.2f \\times %+4.2f " % (mtx[ridx, c], poles[c]))
		result += (" = & \\bmrm{y}_{%d} & = & %+4.2f \\\\\n" % (ridx, output[ridx]))
	result += "\\end{array}\\label{eq:unrolled_filter}\n\\end{equation}"
	return result
