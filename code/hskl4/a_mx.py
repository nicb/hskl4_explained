import numpy as np
import hskl4.A_mx as A_mx
import hskl4.A_mppinv as A_mppinv
import hskl4.seq as hseq
import hskl4.latex as l

def generate():
	A_dag = A_mppinv.generate()
	b = hseq.generate()
	a = np.dot(A_dag, b.T)
	return a

def check():
	A = A_mx.generate()
	b = hseq.generate()
	a = np.linalg.lstsq(A, b.T, rcond=None)[0]
	return a
	
def latex(fun = generate, label = "a_factors"):
	a = fun()
	(ncols,) = a.shape
	result = "\\begin{equation}\n\t\\bmrm{a} = \\{\\quad"
	for c in range(ncols):
		result += (" a_{%d} = %+.8f \quad" % (c, a[c]))
	result += ("\\}\n\\label{eq:%s}\\end{equation}" % (label))
	return result
