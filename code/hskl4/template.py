import os,sys
import pdb
import pystache as ps

class Template:

	def __init__(self, orig_name):
		self.template_prefix = os.path.splitext(os.path.basename(orig_name))[0]
		template_file = self.template_prefix + ".tex.in"
		image_file = self.template_prefix + ".png"
		base_dir = os.path.dirname(os.path.abspath(orig_name))
		self.template_path = os.path.join(base_dir, 'templates', template_file)
		self.image_path = os.path.join(base_dir, '..', 'images', image_file)
		with open(self.template_path, 'r') as stream:
			self.template_text = ''.join(stream.readlines())

	def render(self, values):
		r = ps.Renderer(escape = lambda u: u)
		return r.render(self.template_text, values)
