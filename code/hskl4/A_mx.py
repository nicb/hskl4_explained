import numpy as np
import hskl4 as h
import hskl4.seq as hseq
import hskl4.latex as l
import pdb

def generate():
	seq = hseq.generate()
	nrows  = h.get_source_sample_no()
	ncols  = h.get_npoles()
	result = np.zeros((nrows, ncols), dtype=np.float64)
	for r in range(nrows):
		row = np.zeros((1, ncols), dtype=np.float64)
		# pdb.set_trace()
		for c in range(ncols):
			idx = -c - 1 + r
			if idx < 0:
				row[0][c] = 0.0
			else:
				row[0][c] = seq[idx]
		result[r] = row

	return result

def latex(A, taken = 6):
	(nrows, ncols) = A.shape
	result = "\\begin{equation}\n\tA = \\begin{bmatrix}\n"
	#
	# take the first "taken" rows and the last 6 rows
	#
	result += l.short_rows(A, 0, taken, ncols)
	result += "\t\t\\vdots\\\\\n"
	result += l.short_rows(A, nrows - taken, nrows, ncols)
	result += "\t\\end{bmatrix}\\label{eq:A_matrix}\n\\end{equation}"
	return result
