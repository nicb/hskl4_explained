import numpy as np
import hskl4 as h
import hskl4.A_mx as A_mx
import hskl4.latex as l

def generate():
	A = A_mx.generate()
	A_mppinv = np.dot(np.dot(A.T, A)**(-1), A.T)
	return A_mppinv

def check():
	A = A_mx.generate()
	A_mppinv = np.linalg.pinv(A)
	return A_mppinv

def latex(fun = generate, label = "A_mp_pinv"):
	A_mppinv = fun()
	(nrows, ncols) = A_mppinv.shape
	result = "\\begin{equation}\n\tA^{\\dagger} = \\begin{bmatrix}\n"
	result += l.short_cols(A_mppinv, 0, 3, ncols-3, ncols, nrows)
	result += ("\t\\end{bmatrix}\\label{eq:%s}\n\\end{equation}" % (label))
	return result
