import os,sys
import pdb
import numpy as np
import scipy as sc
import matplotlib.pyplot as plt

import hskl4          as h
import hskl4.template as t
import hskl4.seq      as hseq

tmpl = t.Template(__file__)
D = {}

s = hseq.generate()
D['seq'] = hseq.latex(s)
D['numsamples'] = h.get_source_sample_no()

plt.stem(s)
imgfile = os.path.splitext(os.path.basename(sys.argv[0]))[0] + "_0.png"
plt.savefig("../images/%s" % (imgfile), bbox_inches = 'tight')

print(tmpl.render(D))
