import os,sys
import pdb
import numpy as np
import scipy as sc
import matplotlib.pyplot as plt

import hskl4.seq       as seq
import hskl4.A_mx      as Amx
import hskl4.latex     as l
import hskl4.mppinv    as mpp
import hskl4.template  as t
import hskl4.pulse     as p
import hskl4.filter    as f

tmpl = t.Template(__file__)
D = {}
fig_basename = os.path.splitext(os.path.basename(sys.argv[0]))[0]

#
# calculate the pseudo-inverse of our A matrix
#
A_matrix = Amx.generate()
A_mppinv = mpp.mppinv(A_matrix)

#
# get our original sequence, in 1xN (1-rows, N column) order
#
sig = seq.generate()
sigsz = len(sig)
b = sig.T
a = A_mppinv @ b

#
# create a pulse signal
#
sample_rate = sigsz
pperiod = 16
freq = sample_rate/pperiod
D['pulse_freq'] = freq
D['pulse_period'] = pperiod
t, pexc = p.generate(freq)

plt.stem(pexc)
imgfile = fig_basename + "_0.png"
plt.savefig("../images/%s" % (imgfile), bbox_inches = 'tight')
#
# let's guess what the frequency would be in Hz
#
virtual_sample_rate = 48000  # per sec.
virtual_frequency = (virtual_sample_rate/sample_rate) * freq

D['virtual_sample_rate'] = virtual_sample_rate
D['virtual_frequency'] = virtual_frequency

#
# filter the pulse signal with the found coefficients
#
output = f.filter(pexc, a)
D['filtering_teared_out'] = f.latex_teared_apart(pexc, a, sig)

plt.stem(output)
imgfile = fig_basename + "_1.png"
plt.savefig("../images/%s" % (imgfile), bbox_inches = 'tight')

print(tmpl.render(D))
