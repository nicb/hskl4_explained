import os,sys
import pdb
import numpy as np
import scipy as sc
import matplotlib.pyplot as plt

import hskl4           as h
import hskl4.template  as t
import hskl4.seq       as hseq
import hskl4.matricize as m

tmpl = t.Template(__file__)
D = {}

s = hseq.generate()
mtx = m.matricize(s)
D['matricized_x_with_poles'] = m.latex_with_poles(s, mtx)
single_case = 3
D['single_case'] = m.latex_single_case(mtx, single_case)
D['single_case_performed'] = m.latex_single_case_performed(mtx, single_case)

print(tmpl.render(D))
