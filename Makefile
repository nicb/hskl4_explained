TARGET=hskl4_explained.pdf
SOURCE=$(TARGET:.pdf=.tex)
DEPENDS=values.tex st_svd_ex.tex sect5_our_case.tex sect1_introduction.tex\
	sect2_eq3b.tex sect3_s4.tex sect5_our_case.tex sect6_reconstructing.tex
LATEX=pdflatex
CODEDIR=./code


all: $(TARGET)

$(TARGET): $(SOURCE) $(DEPENDS)

$(DEPENDS): code

code:
	$(MAKE) -C $(CODEDIR) -$(MAKEFLAGS)

clean:
	$(MAKE) -C $(CODEDIR) $@
	$(RM) *.aux *.dvi *.out *.log *.blg *.bbl $(TARGET)

.PHONY: code clean

.SUFFIXES: .pdf .tex

%.pdf:  %.tex
	$(LATEX) $<
	$(RM) $@
	$(LATEX) $<
